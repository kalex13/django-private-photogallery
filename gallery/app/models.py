# coding: utf-8

import os
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from nnmware.core.abstract import AbstractName, AbstractDate
from nnmware.core.models import NnmwareUser
from nnmware.core.imgutil import remove_thumbnails, remove_file

class GalleryUser(NnmwareUser):
    read_only = models.BooleanField(verbose_name=_('Read only user'), blank=True, default=False)


class GalleryAlbum(AbstractDate, AbstractName):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, verbose_name=_("Author"))
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name=_('Users'),
                                   related_name='album_users')
    class Meta:
        ordering = ['name']
        verbose_name = _('Album')
        verbose_name_plural = _('Albums')

    def get_absolute_url(self):
        return reverse('main', kwargs={'pk': self.pk})


def get_image_path(instance, filename):
    instance.title = str(filename)[:-4]
    return "photofile/%s/%s" % (instance.album.pk, filename)


@python_2_unicode_compatible
class GalleryPhoto(models.Model):
    album = models.ForeignKey(GalleryAlbum)
    image = models.ImageField(verbose_name=_('Photo'), upload_to=get_image_path)
    title = models.CharField(verbose_name=_('Title photo'), max_length=255, blank=True, default='')

    class Meta:
        ordering = ['title']
        verbose_name = _('Photo')
        verbose_name_plural = _('Photos')

    def __str__(self):
        return _("%s") % self.title

    def delete(self, *args, **kwargs):
        try:
            remove_thumbnails(self.image.path)
            remove_file(self.image.path)
        except:
            pass
        super(GalleryPhoto, self).delete(*args, **kwargs)