# -*- coding: utf-8 -*-

import os
import re
import mimetypes
from PIL import Image
from hashlib import md5
from datetime import datetime
from io import FileIO, BufferedWriter
from django.contrib.auth import get_user_model
from django.conf import settings
from django.http import HttpResponse
from django.utils.text import slugify
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from nnmware.core.ajax import ajax_answer_lazy
from nnmware.core.exceptions import AccessError
from nnmware.core.imgutil import remove_file, make_thumbnail

from models import GalleryAlbum, GalleryPhoto, GalleryUser


def get_user_from_pk(pk):
    return GalleryUser.objects.get(pk=int(pk))


def get_list_all_albums():
    all_albums = GalleryAlbum.objects.all()
    all_albm = []
    for i in all_albums:
        all_albm.append(i.name)
    return all_albm


def get_album_from_name(album_name):
    return GalleryAlbum.objects.get(name=unicode(album_name))


def get_photo_from_pk(pk):
    return GalleryPhoto.objects.get(pk=int(pk))


def create_album(request):
    try:
        if not request.user.is_authenticated():
            raise AccessError
        name = request.POST['album_name']
        if name.strip() == '':
            payload = {'success': False, 'error': _('Enter the album name')}
            return ajax_answer_lazy(payload)
        # Check for uniqueness of the name
        if GalleryAlbum.objects.filter(name=name):
            payload = {'success': False, 'error': _('Album with the same name already exists')}
            return ajax_answer_lazy(payload)
        slug = slugify(name)
        new_album = GalleryAlbum()
        new_album.name = name
        new_album.slug = slug
        user = request.user
        new_album.user = user
        new_album.save()
        new_album.users.add(user)
        payload = {'success': True, 'name': name, 'pk': new_album.pk}
    except:
        payload = {'success': False, 'error': _('Error create new album')}
    return ajax_answer_lazy(payload)


def cover_set(request, album_pk):
    album= GalleryAlbum.objects.get(pk=int(album_pk))
    uploader = AjaxImageUploader(request, 'covers', is_cover=True)
    result = uploader.file_upload(request)
    if result['success']:
        try:
            remove_file(album.img.url)
        except:
            pass
        album.img = result['path']
        album.save()
        try:
            html = render_to_string('album_cover.html', {'object': album})
            payload = {'success': True, 'html': html}
        except:
            payload = {'success': False}
    else:
        payload = {'success': False, 'error': result['error']}
    return ajax_answer_lazy(payload)


def create_user(request):
    if not request.user.is_superuser:
        raise AccessError
    username = request.POST['username']
    if not username:
        payload = {'success': False, 'error': _('Username is required')}
        return ajax_answer_lazy(payload)
    if not re.match("^[A-Za-z0-9_-]*$", username):
        payload = {'success': False, 'error': _('Incorrect letters in username')}
        return ajax_answer_lazy(payload)
    if len(username) < 5:
        payload = {'success': False, 'error': _('Minimum - 5 letters')}
        return ajax_answer_lazy(payload)
    if len(username) > 16:
        payload = {'success': False, 'error': _('Maximum - 16 letter')}
        return ajax_answer_lazy(payload)
    try:
        get_user_model().objects.get(username=username)
        payload = {'success': False, 'error': _('Username is already used')}
        return ajax_answer_lazy(payload)
    except:
        pass
    password = request.POST['password']
    if not password:
        payload = {'success': False, 'error': _('Password is required')}
        return ajax_answer_lazy(payload)
    if not re.match("^[A-Za-z0-9_-]*$", password):
        payload = {'success': False, 'error': _('Incorrect letters in password')}
        return ajax_answer_lazy(payload)
    if len(password) < 5:
        payload = {'success': False, 'error': _('Minimum - 5 letters')}
        return ajax_answer_lazy(payload)
    try:
        u = get_user_model()(username=username)
        u.set_password(password)
        u.is_active = True
        u.last_login = datetime.now()
        u.read_only = True
        u.save()
        payload = {'success': True, 'username': username, 'pk': u.pk}
    except:
        payload = {'success': False, 'error': _('Error save new user')}
    return ajax_answer_lazy(payload)


def delete_photo(request):
    try:
        if not request.user.is_authenticated():
            raise AccessError
        photos = request.POST.getlist('photo')
        for photo_pk in photos:
            photo = get_photo_from_pk(photo_pk)
            photo.delete()
        payload = {'success': True}
    except:
         payload = {'success': False, 'error': _('Error delete photo')}
    return ajax_answer_lazy(payload)


def download_photo(request, photo_pk):
    photo = GalleryPhoto.objects.get(pk=int(photo_pk))
    image_path = os.path.join(settings.MEDIA_ROOT, photo.image.path)
    image_file = open(image_path, 'rb')
    image_data = image_file.read()
    content_type = mimetypes.guess_type(image_path)[0]
    response = HttpResponse(image_data, content_type=content_type)
    response['Content-Length']      = os.path.getsize(image_path)
    response['Content-Disposition'] = "attachment; filename=%s" % photo.image.name
    return response


def info_user(request, user_pk):
    if request.user.is_superuser:
        try:
            usr = get_user_from_pk(user_pk)
            albums = usr.album_users.all()
            albm = []
            for i in albums:
                albm.append(i.name)
            payload = {'success': True,
                       'username': usr.username,
                       'first_name': usr.first_name,
                       'last_name': usr.last_name,
                       'is_active': usr.is_active,
                       'is_superuser': usr.is_superuser,
                       'read_only': usr.read_only,
                       'albums': albm,
                       'all_albums': get_list_all_albums(),
                       'pk': usr.pk,
                        }
        except:
            payload = {'success': False,'error': _('Error get user')}
    else:
        payload = {'success': False, 'error': _('User is not superuser')}
    return ajax_answer_lazy(payload)

def save_setting(request, album_pk):
    try:
        album= GalleryAlbum.objects.get(pk=int(album_pk))
        album_name = request.POST['album_name']
        album.name = album_name
        slug = slugify(album_name)
        album.slug = slug
        album.description = request.POST['comment']
        album.save()
        users = request.POST.getlist('users')
        album.users.clear()
        for _user in users:
            album.users.add(get_user_from_pk(_user))
        payload = {'success': True, 'name': album_name}
    except:
        payload = {'success':False, 'error': 'Error saved album settings'}
    return ajax_answer_lazy(payload)


def save_user(request, user_pk):
    try:
        if not request.user.is_superuser:
            raise AccessError
        usr = get_user_from_pk(user_pk)
        usr.username = request.POST['username']
        usr.first_name = request.POST['firstname']
        usr.last_name = request.POST['lastname']
        system_settings = request.POST.getlist('system_setting')
        if  'is_active' in system_settings:
            usr.is_active = True
        else:
            usr.is_active = False
        if  'is_superuser' in system_settings:
            usr.is_superuser = True
        else:
            usr.is_superuser = False
        if  'is_read_only' in system_settings:
            usr.read_only = True
        else:
            usr.read_only = False
        usr.save()
        available_albums = request.POST.getlist('albums')
        usr.album_users.clear()
        for alb in available_albums:
            usr.album_users.add(get_album_from_name(alb))
        payload = {'success': True, 'username': usr.username}
    except:
        payload = {'success': False, 'error': _('Error save user setting')}
    return ajax_answer_lazy(payload)


def upload_album(request, album_pk):
    album= GalleryAlbum.objects.get(pk=int(album_pk))
    try:
        html = render_to_string('album_cover.html', {'object': album})
        payload = {'success': True, 'html': html, 'name': album.name}
    except:
        payload = {'success': False}
    return ajax_answer_lazy(payload)


def upload_photo(request, album_pk):
    new_photo = GalleryPhoto()
    # Check for uniqueness of the name
    photo_name = request.POST['qqfilename'].split('.')[0]
    if GalleryPhoto.objects.filter(album_id=album_pk, title=photo_name):
        payload = {'success': False, 'error': _('Photo with the same name already upload')}
        return ajax_answer_lazy(payload)
    uploader = AjaxImageUploader(request, 'photofile/' + album_pk)
    result = uploader.file_upload(request)
    if result['success']:
        new_photo.album_id = album_pk
        new_photo.image = result['path']
        new_photo.title = photo_name
        new_photo.save()
        make_thumbnail(new_photo.image.url, width=1280, height=960)
        make_thumbnail(new_photo.image.url, width=238,height=178)
        payload = {'success': True, 'name': photo_name}
    else:
        payload = {'success': False, 'error': result['error']}
    return ajax_answer_lazy(payload)


class AjaxImageUploader(object):
    BUFFER_SIZE = 10485760  # 10MB

    def __init__(self, request, upload_dir, is_cover=False):
        self._upload_dir = os.path.join(settings.MEDIA_ROOT, upload_dir)
        if is_cover:
            filename = request.POST['qqfilename']
            ext = os.path.splitext(filename)[1]
            self._filename = md5(filename.encode('utf8')).hexdigest() + ext
        else:
            self._filename = request.POST['qqfilename']
        self._path = os.path.join(self._upload_dir, self._filename)
        self._is_cover = is_cover

    def file_upload(self, request):
        MAX_SIDE = 240
        MIN_SIDE = 180
        if not os.path.exists(os.path.realpath(os.path.dirname(self._path))):
            try:
                os.makedirs(os.path.realpath(os.path.dirname(self._path)))
            except:
                return dict(success=False, error=_("Error make dir:" + self._path))
        if request.FILES:
            if len(request.FILES) == 1:
                upload = request.FILES.values()[0]
            else:
                return dict(success=False, error=_("Bad upload."))
        else:
            upload = request
        self._destination = BufferedWriter(FileIO(self._path, "w"))
        try:
            chunk = upload.read(self.BUFFER_SIZE)
            while len(chunk) > 0:
                self._destination.write(chunk)
                chunk = upload.read(self.BUFFER_SIZE)
        except:
            return dict(success=False, error=_("Upload error"))
        self._destination.close()
        if self._is_cover:
            try:
                cover = Image.open(self._path)
            except:
                return dict(success=False, error=_("File cover is not image format"))
            width, height = cover.size
            if width > height:
                prev_height = (MAX_SIDE*height)/width
                prev_cover = cover.resize((MAX_SIDE, prev_height))
            else:
                prev_width = (MIN_SIDE*width)/height
                prev_cover = cover.resize((prev_width, MIN_SIDE))
            prev_cover.save(self._path)
        return dict(success=True, fullpath=self._path, path=os.path.relpath(self._path, '/' + settings.MEDIA_ROOT),
                    filename=self._filename)