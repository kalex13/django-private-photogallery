# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from models import GalleryUser, GalleryAlbum, GalleryPhoto


@admin.register(GalleryUser)
class GalleryUserAdmin(UserAdmin):
    list_display = ('username', 'is_active', 'is_superuser', 'read_only', 'last_login')

    fieldsets = (
        (None, {'fields': [('username', 'read_only')]}),
        ("Profile", {"classes": ("grp-collapse grp-closed",), "fields": [('first_name', ),('last_name')]}),
        ('System settings (!!!)', {"classes": ("grp-collapse grp-closed",), 'fields': [('is_active', 'is_superuser'),
                                                                                       ('last_login', 'date_joined'),
                                                                                       ('password',)]}),
    )
    list_filter = ('is_active',)


@admin.register(GalleryAlbum)
class GalleryAlbumAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'img', 'slug', 'user']}),
        ("Users", {"classes": ("grp-collapse grp-closed",), "fields": [('users',)]}),
    ]
    list_display = ['name', 'img', 'slug']
    prepopulated_fields = {'slug': ['name']}
    ordering = ['name']
    filter_horizontal = ['users']


@admin.register(GalleryPhoto)
class GalleryPhotoAdmin(admin.ModelAdmin):

    fieldsets = [
        (None, {"fields": ['album', 'image']})
    ]
    list_display = ['title', 'album', 'image']
    ordering = ['title']

