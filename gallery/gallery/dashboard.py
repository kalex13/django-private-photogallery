# -*- coding: utf-8 -*-

from grappelli.dashboard import modules, Dashboard


class GalleryDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)

        self.children.append(modules.AppList(
            title='Gallery.my',
            column=1,
            collapsible=True,
            models=('app.*',)
        ))
        self.children.append(modules.RecentActions(
            'Recent Actions',
            limit=10,
            column=2,
        ))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            'Gallery site',
            column=3,
            children=[
                {
                    'title': 'Main Page',
                    'url': '/',
                    'external': False,
                },
                {
                    'title': 'Admin page',
                    'url': '/admin/',
                    'external': False,
                },
            ]
        ))


